<p align="center">
<a href="https://typora.io/"><img src="https://typora.io/img/icon_512x512.png" alt="typora icon" width="150" /></a>
</p>

## Customize

**字体大小**: <kbd>设置界面</kbd> -> <kbd>外观</kbd> -> <kbd>字体大小</kbd> -> <kbd>设置字体大小</kbd>

- 自定义字体请编辑此处 [font.css](https://github.com/liangjingkanji/DrakeTyporaTheme/blob/master/drake/font.css#L35-L37)
- 行高/段间距请编辑此处 [font.css](https://github.com/liangjingkanji/DrakeTyporaTheme/blob/master/drake/font.css#L38-L40)
- 更多请查看[常见问题](https://github.com/liangjingkanji/DrakeTyporaTheme/blob/master/issues.md)

<br>

> 如果发生符号粘连, 那是主题内置字体JetBrainsMono的特性 `ligatures`, 如不喜欢可自行删除: [#75](https://github.com/liangjingkanji/DrakeTyporaTheme/issues/75)

<br>

| 推荐字体                                                     | 描述                                                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| [JetBrains Mono](https://www.jetbrains.com/zh-cn/lp/mono/)   | 英文字体, 适合开发人员的字体, 修改版本: [JetBrainsMono-patch](https://github.com/liangjingkanji/JetBrainsMono-patch) |
| [Fira Code](https://github.com/tonsky/FiraCode)              | 英文字体, 前端开发人员喜欢用的字体                           |
| [cascadia-code](https://github.com/microsoft/cascadia-code)  | 英文字体, 微软官方字体, Windows Terminal的默认字体, 修改版本: [cascadia-code-patch](https://github.com/liangjingkanji/cascadia-code-patch) |
| [PTCode](https://github.com/liangjingkanji/PTCode)           | PT Mono 增加连字特性(Ligatures)                              |
| [PlexMono](https://github.com/liangjingkanji/PlexMono)       | IBM Plex Mono 增加连字特性(Ligatures)                        |
| [HYZhengYuan](http://www.hanyi.com.cn/productdetail?id=2915) | 中文字体, 汉仪正圆                                           |


## Install

- [x] 首先确定已安装[Typora](https://typora.io/)

- [ ] 通过`设置 -> 外观 -> 打开主题文件夹`打开theme目录

- [ ] 复制所有文件到`theme`目录下然后重启, 选择菜单栏 -> 主题即可

